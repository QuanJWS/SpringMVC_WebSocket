# SpringMVC_WebSocket

#### 介绍

spring+springMVC+WebSocket 的学习笔记

#### 软件架构
软件架构说明

1. JDK1.8
2. SpringMVC 4.0.2.RELEASE
3. spring-websocket 4.0.2.RELEASE



#### 安装教程

1. 下载后打包war放入Tomcat


#### 使用说明

1. 此次使用了两种方式实现spring+websocket整合
    1. xml配置方式
    2. 注解配置方式

2. 在使用xml配置方式时，把springMVC.xml中包扫描注释
    
    ~~~
    <context:component-scan base-package="com.cuit.secims.mw.ws" />
    

3. 在使用注解配置方式时，把springMVC.xml中导入spring_websocket.xml代码注释
    
    ~~~
    <import resource="classpath:spring_WebSocket.xml"/>

4. 项目根路径项目名称要在index.js中修改成自己项目名称
    
    ~~~
    // 首先判断是否 支持 WebSocket
    if('WebSocket' in window) {
        websocket = new WebSocket("ws://localhost:8080/SpringMVC_WebSocket/websocket");
    } else if('MozWebSocket' in window) {
        websocket = new MozWebSocket("ws://localhost:8080/SpringMVC_WebSocket/websocket");
    } else {
        websocket = new SockJS("http://localhost:8080/SpringMVC_WebSocket/sockjs/websocket");
    }

    此处的 SpringMVC_WebSocket 就是项目名称，要修改成自己的项目名称。



